#!/bin/bash

#export OTPROOT="$LANG_PATH/dist"

################################################################################

ronin_require_erlang () {
    echo hello world >/dev/null
}

ronin_include_erlang () {
    motd_text "    -> Erlang    : "$OTPROOT
}

################################################################################

ronin_setup_erlang () {
    echo hello world >/dev/null
}

